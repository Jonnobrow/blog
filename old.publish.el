;; Load the templating framework
(add-to-list 'load-path "./")
(load "ox-thtml.el")

(setq org-publish-project-alist
      `(
        ("homepage-blog"
         :base-directory ,(expand-file-name "./blog")
         :root-directory ,(expand-file-name "./")
         :recursive t
         :base-extension "org"
         :publishing-directory ,(expand-file-name "./public_html/blog")
         ;; Exclude the blog archive index autogenerated below
         ;; Note that the regexp is relative to :base-directory
         :exclude "^index.org"
         :section-numbers nil
         :with-toc nil
         :with-date nil
         :html-template ,(templated-html-load-template "templates/blog.html")
         :publishing-function org-html-publish-to-templated-html
         :auto-sitemap t
         :sitemap-folders ignore
         :sitemap-style list
         :sitemap-title "Jonathan Bartlett's Website"
         :sitemap-filename "sitemap.inc"
         :sitemap-sort-files anti-chronologically
         )
        ("homepage-pages"
         :base-directory ,(expand-file-name "./")
         :recursive t
         :base-extension "org"
         :include ("blog/index.org")
         :exclude ,(regexp-opt '("blog"))
         :publishing-directory ,(expand-file-name "./public_html")
         :section-numbers nil
         :with-toc nil
         :with-date nil
         :html-template ,(templated-html-load-template "templates/index.html")
         :publishing-function org-html-publish-to-templated-html
         )
        ("homepage-assets"
         :base-directory "./"
         :publishing-directory "./public_html"
         :recursive t
         :exclude "\\(public_html\\|examples\\|templates\\).*"
         :base-extension "jpg\\|gif\\|png\\|css\\|js\\|nb\\|ipynb\\|pdf\\|c\\|zip"
         :publishing-function org-publish-attachment)
        ("homepage" :components ("homepage-blog" "homepage-pages" "homepage-assets"))
        ))

;;
;; Publish the whole site into a subdirectory, as explained above. Start from
;; scratch every time, at least until framework converges
(shell-command "rm -rf public_html")
(org-publish "homepage" t)
(shell-command "/usr/bin/sass sass:public_html/css --style compressed --no-source-map")
